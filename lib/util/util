#!/bin/sh

dir="$(dirname "$(readlink -f "$0")")"

util() {
    "$dir/util" "$@"
}

exit_code() {
    util print return "$?"
}

unquoted() {
    to_unquote="$1"
    to_unquote="${to_unquote%\'}"
    util print return "${to_unquote#\'}"
}

unquoted_dbl() {
    to_unquote="$1"
    to_unquote="${to_unquote%\"}"
    util print return "${to_unquote#\"}"
}

is_null_or_whitespace() {
    [ -z "${1#${1%%*[![:blank:]]}}" ]
}

starts_with() {
    line="$1"
    prefix="$2"
    [ -n "${line%${line#"$prefix"}}" ]
}

is_non_option() {
    case "$1" in
        -*)
            return 1
            ;;
    esac
    ! util is-null-or-whitespace "$1"
}

command="$1"
[ "$#" -gt 0 ] && shift

case "$command" in
    cache)
        "$dir/cache-util" "$@"
        ;;
    io)
        "$dir/io-util" "$@"
        ;;
    net)
        "$dir/net-util" "$@"
        ;;
    print)
        "$dir/print-util" "$@"
        ;;
    exit-code)
        exit_code "$@"
        ;;
    unquoted)
        unquoted "$@"
        ;;
    unquoted-dbl)
        unquoted_dbl "$@"
        ;;
    is-null-or-whitespace)
        is_null_or_whitespace "$@"
        ;;
    starts-with)
        starts_with "$@"
        ;;
    is-non-option)
        is_non_option "$@"
        ;;
    *)
        util print err "util does not exist: $command"
        exit 1
        ;;
esac
