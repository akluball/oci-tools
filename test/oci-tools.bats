#!/usr/bin/env bats

teardown() {
    "$BATS_TEST_DIRNAME/teardown"
}

@test "oci-tools help" {
    run oci-tools help
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = 'Usage:' ]
    grep -E '   oci-tools.*' <<< "${lines[1]}"
}

@test "oci-tools, no COMMAND" {
    run oci-tools
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'COMMAND is required' ]
}

@test "oci-tools unkown COMMAND" {
    run oci-tools unknown-command
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'unknown COMMAND: unknown-command' ]
}
