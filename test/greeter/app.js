const http = require('http');
const os = require('os');

const hostname = os.hostname();
console.log(`output log test from ${hostname}`)
console.error(`error log test from ${hostname}`)

function handler(req, res) {
    res.writeHead(200);
    res.end(`hello world from ${hostname}`);
}

http.createServer(handler)
    .listen(80);
