#!/usr/bin/env bats

util() {
    "$BATS_TEST_DIRNAME/util" "$@"
}

test_utils() {
    "$BATS_TEST_DIRNAME/test-utils" "$@"
}

teardown() {
    "$BATS_TEST_DIRNAME/teardown"
}

@test "bundle help" {
    run oci-tools bundle help
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = 'Usage:' ]
    grep -E "   bundle.*" <<< "${lines[1]}"
}

@test "bundle no COMMAND" {
    run oci-tools bundle
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'COMMAND is required' ]
}

@test "bundle unknown COMMAND" {
    run oci-tools bundle unknown-command
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'unknown COMMAND: unknown-command' ]
}

@test "bundle extract help" {
    run oci-tools bundle extract help
    [ "${lines[0]}" = 'Usage:' ]
    grep -E "    bundle extract.*" <<< "${lines[1]}"
}

@test "bundle extract, unknown option" {
    run oci-tools bundle extract --unknown-option "$(test_utils greeter-tarball)"
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'unknown option encountered: --unknown-option' ]
}

@test "bundle extract, missing --name optarg" {
    run oci-tools bundle extract "$(test_utils greeter-tarball)" --name
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'missing required option argument for --name' ]
}

@test "bundle extract, no IMAGE-TARBALL" {
    run oci-tools bundle extract
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'IMAGE-TARBALL is required' ]
}

@test "bundle extract, multiple IMAGE-TARBALL" {
    image_tarball="$(test_utils greeter-tarball)"
    tmp_dir="$(mktemp -d)"
    other_image_tarball="$tmp_dir/other-greeter.tar"
    cp "$image_tarball" "$other_image_tarball"
    run oci-tools bundle extract "$image_tarball" "$other_image_tarball"
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'multiple IMAGE-TARBALL not supported' ]
    rm -r "$tmp_dir"
}

@test "bundle extract, bad --format option" {
    run oci-tools bundle extract --format not-a-format "$(test_utils greeter-tarball)"
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'unknown image format: not-a-format' ]
}

@test "bundle extract, not a tarball" {
    not_a_tarball="$(mktemp)"
    run oci-tools bundle extract --name greeter1 "$not_a_tarball"
    [ "$status" -ne 0 ]
    grep -E 'unable to extract.*' <<< "${lines[0]}"
    rm "$not_a_tarball"
    [ ! -d "$(util cache bundle greeter1)" ]
}

create_invalid_image() {
    image_tarball="$(test_utils greeter-tarball)"
    extracted_image="$(mktemp -d)"
    invalid_image_tarball="$(mktemp -u --suffix '.tar')"
    tar -x -f "$image_tarball" -C "$extracted_image"
    rm -r "$extracted_image/blobs"
    tar -c -f "$invalid_image_tarball" -C "$extracted_image" .
    rm -r "$extracted_image"
    util print return "$invalid_image_tarball"
}

@test "bundle extract, bad oci image" {
    invalid_image_tarball="$(create_invalid_image)"
    run oci-tools bundle extract --name greeter1 "$invalid_image_tarball"
    [ "$status" -ne 0 ]
    grep -E 'unable to unpack image.*' <<< "${lines[0]}"
    [ ! -d "$(util cache bundle greeter1)" ]
    rm -r "$invalid_image_tarball"
}

@test "bundle extract, name clash" {
    # fake existing bundle
    mkdir -p "$(util cache bundle greeter1)"
    run oci-tools bundle extract --name greeter1 "$(test_utils greeter-tarball)"
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'bundle with name greeter1 already exists' ]
}

@test "bundle delete help" {
    run oci-tools bundle delete help
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = 'Usage:' ]
    grep -E "    bundle delete.*" <<< "${lines[1]}"
}

@test "bundle delete, no BUNDLE-NAME" {
    run oci-tools bundle delete
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'BUNDLE-NAME is required' ]
}

@test "bundle delete, multiple BUNDLE-NAME" {
    image_tarball="$(test_utils greeter-tarball)"
    oci-tools bundle extract --name greeter1 "$image_tarball"
    oci-tools bundle extract --name greeter2 "$image_tarball"
    oci-tools bundle delete greeter1 greeter2
    [ ! -d "$(util cache bundle greeter1)" ]
    [ ! -d "$(util cache bundle greeter2)" ]
}
