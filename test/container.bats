#!/usr/bin/env bats

test_utils() {
    "$BATS_TEST_DIRNAME/test-utils" "$@"
}

util() {
    "$BATS_TEST_DIRNAME/util" "$@"
}

teardown() {
    "$BATS_TEST_DIRNAME/teardown"
}

@test "container help" {
    run oci-tools container help
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = 'Usage:' ]
    grep -E "    container.*" <<< "${lines[1]}"
}

@test "container unknown COMMAND" {
    run oci-tools container unknown-command
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'unknown COMMAND: unknown-command' ]
}

@test "container, no COMMAND" {
    run oci-tools container
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'COMMAND is required' ]
}

@test "container run help" {
    run oci-tools container run help
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = 'Usage:' ]
    grep -E "    container run.*" <<< "${lines[1]}"
}

@test "container run unknown option" {
    run oci-tools container run --detach --unknown-option greeter
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'unknown option encountered: --unknown-option' ]
}

@test "container run, missing --network optarg" {
    run oci-tools container run --detach greeter --network
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'missing required option argument for --network' ]
}

@test "container run, no CONTAINER-NAME" {
    run oci-tools container run
    [ "${lines[0]}" = 'CONTAINER-NAME is required' ]
}

@test "container run, multiple CONTAINER-NAME" {
    run oci-tools container run --detach greeter1 greeter2
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'multiple CONTAINER-NAME not supported' ]
}

@test "container run, non existent bundle" {
    run oci-tools container run --detach --bundle dne greeter
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'bundle not found: dne' ]
}

@test "container create, name clash" {
    # fake existing container
    mkdir -p "$(util cache container-root)/greeter1"
    run oci-tools container run --detach greeter1
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'container with name greeter1 already exists' ]
}

@test "container rwait help" {
    run oci-tools container rwait help
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = "Usage:" ]
    grep -E "    container rwait.*" <<< "${lines[1]}"
}

@test "container rwait, unknown option" {
    run oci-tools container rwait --unknown-option greeter
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'unknown option encountered: --unknown-option' ]
}

@test "container rwait, no CONTAINER-NAME" {
    run oci-tools container rwait
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'CONTAINER-NAME is required' ]
}

@test "container rwait, multiple CONTAINER-NAME" {
    run oci-tools container rwait greeter1 greeter2
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'multiple CONTAINER-NAME not supported' ]
}

@test "container delete help" {
    run oci-tools container delete help
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = 'Usage:' ]
    grep -E "    container delete.*" <<< "${lines[1]}"
}

@test "container delete, no CONTAINER-NAME" {
    run oci-tools container delete
    [ "${lines[0]}" = 'CONTAINER-NAME is required' ]
}
