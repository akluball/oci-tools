#!/usr/bin/env bats

util() {
    "$BATS_TEST_DIRNAME/util" "$@"
}

test_utils() {
    "$BATS_TEST_DIRNAME/test-utils" "$@"
}

teardown() {
    "$BATS_TEST_DIRNAME/teardown"
}

@test "one-time-greeter workflow" {
    oci-tools bundle extract --format docker "$(test_utils one-time-greeter-tarball)"
    run oci-tools container run --env WHO_I_AM=human one-time-greeter
    [ "$status" -eq 0 ]
    [ "$output" = 'hello human' ]
    oci-tools bundle delete one-time-greeter
}

@test "default rwait and output logs" {
    oci-tools network create --ipv4-addr 10.0.0.1/24 oci0
    oci-tools bundle extract "$(test_utils greeter-tarball)"
    oci-tools container run --detach --network oci0 greeter
    oci-tools container rwait greeter
    curl -4sS greeter
    grep -E 'output log test from greeter' < "$(util cache container greeter)/log/out"
    grep -E 'error log test from greeter' < "$(util cache container greeter)/log/err"
    oci-tools container delete greeter
    oci-tools bundle delete greeter
    oci-tools network delete oci0
}

@test "ipv4 and ipv6 workflow" {
    oci-tools network create --domain test --ipv4-addr 10.0.0.1/24 --ipv6-addr fd00::1/64 oci0
    oci-tools bundle extract "$(test_utils greeter-tarball)"
    oci-tools container run --detach --network oci0 greeter
    oci-tools container rwait -4 -6 greeter
    [ "$(curl -4sS greeter.test)" = 'hello world from greeter' ]
    [ "$(curl -6sS greeter.test)" = 'hello world from greeter' ]
    oci-tools container delete greeter
    oci-tools bundle delete greeter
    oci-tools network delete oci0
}

@test "container dns workflow" {
    oci-tools network create --domain test --ipv4-addr 10.0.0.1/24 --ipv6-addr fd00::1/64 oci0
    oci-tools bundle extract "$(test_utils greeter-tarball)"
    oci-tools container run --detach --network oci0 --bundle greeter greeter1
    oci-tools container run --detach --network oci0 --bundle greeter greeter2
    oci-tools container rwait -4 -6 greeter1
    oci-tools container rwait -4 -6 greeter2
    [ "$(runc exec greeter1 curl -4sS greeter2)" = 'hello world from greeter2' ]
    [ "$(runc exec greeter2 curl -6sS greeter1)" = 'hello world from greeter1' ]
    [ "$(runc exec greeter1 curl -4sS --connect-timeout 5 greeter1)" = 'hello world from greeter1' ]
    oci-tools container delete greeter1 greeter2
    [ ! -d "$(util cache container greeter2)" ]
    oci-tools bundle delete greeter
    oci-tools network delete oci0
}
