#!/usr/bin/env bats

util() {
    "$BATS_TEST_DIRNAME/util" "$@"
}

@test "system help" {
    run oci-tools system help
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = 'Usage:' ]
    grep -E '    system.*' <<< "${lines[1]}"
}

@test "system, unknown command" {
    run oci-tools system unknown-command
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'unknown system COMMAND: unknown-command' ]
}

@test "system clean-dns" {
    oci-tools system clean-dns
    resolv_conf="$(cat /etc/resolv.conf)"
    util is-null-or-whitespace "$resolv_conf"
}