#!/bin/bash

@test "deps help" {
    run oci-tools deps help
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = 'Usage:' ]
    grep -E '    deps.*' <<< "${lines[1]}"
}

@test "deps, unknown COMMAND" {
    run oci-tools deps unknown-command
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'unknown COMMAND: unknown-command' ]
}

@test "deps, no COMMAND" {
    run oci-tools deps
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'COMMAND is required' ]
}