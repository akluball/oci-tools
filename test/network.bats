#!/usr/bin/env bats

util() {
    "$BATS_TEST_DIRNAME/util" "$@"
}

test_utils() {
    "$BATS_TEST_DIRNAME/test-utils" "$@"
}

teardown() {
    "$BATS_TEST_DIRNAME/teardown"
}

@test "network help" {
    run oci-tools network help
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = 'Usage:' ]
    grep -E "    network.*" <<< "${lines[1]}"
}

@test "network, unknown COMMAND" {
    run oci-tools network unknown-command
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'unknown COMMAND: unknown-command' ]
}

@test "network, no COMMAND" {
    run oci-tools network
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'COMMAND is required' ]
}

@test "network create help" {
    run oci-tools network create help
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = 'Usage:' ]
    grep -E "    network create.*" <<< "${lines[1]}"
    # make sure a network named help was not created
    run ip link show help
    [ "$status" -ne 0 ]
}

@test "network create --unknown-option" {
    run oci-tools network create --unknown-option oci0
    [ "${lines[0]}" = 'unknown option encountered: --unknown-option' ]
}

@test "network create, missing --domain optarg" {
    run oci-tools network create oci0 --domain
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'missing required option argument for --domain' ]
}

@test "network create, no NETWORK-NAME" {
    run oci-tools network create -4 10.0.0.1/24 -6 fd00::/64
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'NETWORK-NAME is required' ]
}

@test "network create, multiple NETWORK-NAME" {
    run oci-tools network create -4 10.0.0.1/24 -6 fd00::/64 net1 net2
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'multiple NETWORK-NAME not supported' ]
}

@test "network create, no ipv4 or ipv6 address" {
    run oci-tools network create oci0
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'either --ipv4-addr,--ipv6-addr or both is required' ]
}

@test "network create, dnsmasq fail" {
    run oci-tools network create -4 10.1/24 oci0
    [  "$status" -ne 0 ]
    [ "${lines[0]}" = 'unable to start dnsmasq, network not created' ]
    [ ! -d "$(util cache network oci0)" ]
    run ip link show oci0
    [ "$status" -ne 0 ]
}

@test "network create, name clash" {
    # fake existing network
    mkdir -p "$(util cache network oci0)"
    run oci-tools network create -4 10.0.0.1/24 oci0
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'network with name oci0 already exists' ]
}

@test "network delete help" {
    run oci-tools network delete help
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = 'Usage:' ]
    grep -E "    network delete.*" <<< "${lines[1]}"
}

@test "network delete, no NETWORK-NAME" {
    run oci-tools network delete
    [ "$status" -ne 0 ]
    [ "${lines[0]}" = 'NETWORK-NAME is required' ]
}

@test "network delete, multiple NETWORK-NAME" {
    oci-tools network create -4 10.0.0.1/24 oci0
    oci-tools network create -4 10.0.1.1/24 oci1
    oci-tools network delete oci0 oci1
    # verify networks deleted
    run ip link show oci0
    [ "$status" -ne 0 ]
    run ip link show oci1
    [ "$status" -ne 0 ]
}

@test "network create then delete" {
    oci-tools network create -6 fd00::/64 oci0
    ip link show oci0
    oci-tools network delete oci0
    run ip link show oci0
    [ "$status" -ne 0 ]
}

@test "network delete, clean resolv.conf" {
    oci-tools network create -4 10.0.0.1/24 oci0
    oci-tools network delete oci0
    run grep -E "nameserver 10.0.0.1" < /etc/resolv.conf
    [ "$status" -ne 0 ]
}

@test "network delete, container on network" {
    oci-tools network create -4 10.0.0.1/24 oci0
    oci-tools bundle extract "$(test_utils greeter-tarball)"
    oci-tools container run --detach --network oci0 greeter
    run oci-tools network delete oci0
    [ -d "$(util cache network oci0)" ]
    oci-tools container delete greeter
    oci-tools network delete oci0
    [ ! -d "$(util cache network oci0)" ]
}

@test "network delete, orphan container reference" {
    oci-tools network create -4 10.0.0.1/24 oci0
    util io append greeter "$(util cache network "oci0")/containers"
    run oci-tools network delete oci0
    [ ! -d "$(util cache network oci0)" ]
}
