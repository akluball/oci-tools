FROM alpine:3.10
# test dependencies
RUN apk add --no-cache \
    git \
    bash \
    curl \
    coreutils \
    ncurses
# install bats
RUN git clone --branch v1.1.0 --depth 1 https://github.com/bats-core/bats-core.git /bats
RUN /bats/install.sh /usr
RUN rm -r /bats
# install oci-tools
WORKDIR /oci-tools
COPY lib lib
COPY test test
RUN ln -s /oci-tools/lib/oci-tools /usr/bin
RUN oci-tools deps apk
# test
ENTRYPOINT test/runner