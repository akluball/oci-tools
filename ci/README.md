# OCI Tools Continuous Integration

Continuous integration is performed using [concourse].
The fly client binary must be in path.
A [buildkit] instance is also required.

On a posix host with docker, this infrastructure can be setup for local ci using [ci-side-by-side].

## Infrastructure

To setup with [ci-side-by-side]:
```
sbs network up
sbs concourse up --login <concourse-target>
sbs buildkit up
```

To teardown:
```
sbs buildkit down
sbs concourse down
sbs network down
```

create pipeline (branch defaults to master):
```
./pipeline-up \
    --target <concourse-target> \
    --key-file <path-to-key-file> \
    --buildkit-addr <buildkit-addr> \
    BRANCH
```

destroy pipeline (branch defaults to master):
```
./pipeline-down \
    --target <concourse-target> \
    BRANCH
```

[concourse]: https://concourse-ci.org/
[buildkit]: https://github.com/moby/buildkit

[ci-side-by-side]: https://gitlab.com/admklu/ci-side-by-side
