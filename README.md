# OCI Tools

A unified tool for managing containers powered by low-level tools ([runc], [umoci], ip...).
This tool is meant to be used inside a container (not on the host machine).

## Alpine Container Install

The following commands should be run in the container where [oci-tools] will be used.

clone:
```
git clone git@gitlab.com:akluball/oci-tools.git
```

create symlink:
```
ln -s "$PWD/oci-tools/lib/oci-tools" "/usr/local/bin"
```

install dependencies:
```
oci-tools deps apk
```

mount cgroups:
```
oci-tools system mount-cgroups
```

## Usage

Extract oci image tarball to runtime bundle:
```
oci-tools bundle extract --name <runtime-bundle-name> <path-to-image-tarball>
```

Run container from extracted runtime bundle:
```
oci-tools container run --bundle <runtime-bundle-name> <container-name>
```

Stop and delete running container:
```
oci-tools container delete <container-name>
```

Run `oci-tools help` to see the available commands and `oci-tools <command> help` for help with a given command.

## Development/Debug

build greeter oci image tarball (for extracting to runtime bundle)
```
./test/greeter/build
```

start shell in container:
```
docker run \
    --name oci-tools \
    --rm \
    --interactive --tty \
    --privileged \
    --mount type=bind,source="$(pwd)/lib",target=/oci-tools/lib,readonly \
    --mount type=bind,source="$(pwd)/test",target=/oci-tools/test,readonly \
    --workdir /oci-tools \
    alpine:3.10 \
    sh -c "ln -s /oci-tools/lib/oci-tools /usr/bin && oci-tools deps apk && sh"
```

## Test

Tests are run using [bats] in a [docker] container.
Notable dependencies include [runc] and [umoci].
See [apk-deps] for a more complete list of dependencies.

build image tarballs for testing
```
./test/build-images
```

build test runner image:
```
docker build --tag oci-tools-test .
```

run tests one time:
```
docker run --privileged --rm oci-tools-test
```

to run tests in development, mount test and lib, override entrypoint with shell:
```
docker run \
    --name oci-tools-test \
    --rm \
    --interactive --tty \
    --privileged \
    --mount type=bind,source="$(pwd)/lib",target=/oci-tools/lib,readonly \
    --mount type=bind,source="$(pwd)/test",target=/oci-tools/test,readonly \
    --entrypoint sh \
    oci-tools-test
```

from container shell, run tests using the runner:
```
./test/runner
```

to only run tests against specific files, pass their path (relative to test)
```
./test/runner workflow.bats
```

to cleanup, remove greeter tarball and test runner image
```
rm test/greeter/greeter.tar
docker rmi oci-tools-test
```

[oci-tools]: https://gitlab.com/akluball/oci-tools

[bats]: https://github.com/bats-core/bats-core
[docker]: https://docs.docker.com/install/
[runc]: https://github.com/opencontainers/runc
[umoci]: https://github.com/openSUSE/umoci

[apk-deps]: lib/deps/apk-deps
